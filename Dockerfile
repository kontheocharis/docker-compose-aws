ARG DOCKER_VERSION=20.10
ARG DOCKER_COMPOSE_VERSION=1.29.2
ARG AWS_CLI_VERSION=2.2.9

FROM docker:${DOCKER_VERSION} as docker

FROM docker/compose:debian-${DOCKER_COMPOSE_VERSION}
COPY --from=docker /usr/local/bin/docker /usr/local/bin/docker

SHELL ["/bin/bash", "-c"] 

ARG AWS_CLI_VERSION

RUN apt-get -y update && apt-get -y install curl unzip
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWS_CLI_VERSION}.zip" -o "awscliv2.zip" \
        && unzip awscliv2.zip \
        && ./aws/install

CMD []
